import {
  ActionReducerMap,
  createAction,
  createReducer,
  on,
  props
} from '@ngrx/store';

import { Video } from './dashboard/types';

export const selectVideo = createAction(
  'SELECT_VIDEO',
  props<{video: Video}>()
);

export interface AppState {
  selectedVideo: Video | undefined;
}

export const selectVideoReducer = createReducer(
  undefined,
  on(selectVideo, (_, action) => action.video)
);

export const reducers: ActionReducerMap<AppState> = {
  selectedVideo: selectVideoReducer
};
