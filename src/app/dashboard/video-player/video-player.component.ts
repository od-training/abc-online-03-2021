import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { VideoDataService } from 'src/app/video-data.service';
import { Video } from '../types';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent implements OnInit {
  video: Observable<Video | undefined>;
  url: Observable<SafeResourceUrl>;

  constructor(videoDataService: VideoDataService, sanitizer: DomSanitizer) {
    this.video = videoDataService.selectedVideo;
    this.url = videoDataService.selectedVideo.pipe(
      filter(video => !!video),
      map(video => sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + video.id))
    );
  }

  ngOnInit(): void {
  }

}
