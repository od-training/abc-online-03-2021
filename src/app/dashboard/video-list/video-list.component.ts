import { Component, OnInit } from '@angular/core';
import { Video } from '../types';
import { VideoDataService } from '../../video-data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {
  videoList: Observable<Video[]>;

  selectedVideo: Observable<Video | undefined>;

  constructor(private videoDataService: VideoDataService) {
    this.videoList = this.videoDataService.videoList;
    this.selectedVideo = this.videoDataService.selectedVideo;
  }

  ngOnInit(): void {
  }

  videoSelected(video: Video): void {
    this.videoDataService.selectVideo(video);
  }
}
