import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Video } from './dashboard/types';
import { AppState, selectVideo } from './state';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {
  selectedVideo: Observable<Video | undefined>;
  videoList: Observable<Video[]>;

  constructor(private http: HttpClient, private store: Store<AppState>) {
    this.selectedVideo = this.store.pipe(select(state => state.selectedVideo))
    this.videoList = this.getVideos();
  }

  private getVideos(): Observable<Video[]> {
    return this.http.get<Video[]>('https://api.angularbootcamp.com/videos');
  }

  selectVideo(video: Video): void {
    this.store.dispatch(selectVideo({video}));
  }
}

